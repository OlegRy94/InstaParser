package com.olegry.instaparser.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by olegrabcev on 03.07.16.
 */
public class InstagramPhotoInfo {

    @SerializedName("entry_data")
    private EntryData entryData;

    public EntryData getEntryData() {
        return entryData;
    }

    public void setEntryData(EntryData entryData) {
        this.entryData = entryData;
    }

    public class EntryData {

        @SerializedName("PostPage")
        private List<PostPage> postPage;

        public List<PostPage> getPostPage() {
            return postPage;
        }

        public void setPostPage(List<PostPage> postPage) {
            this.postPage = postPage;
        }

        public class PostPage {

            @SerializedName("media")
            private Media media;

            public Media getMedia() {
                return media;
            }

            public void setMedia(Media media) {
                this.media = media;
            }

            public class Media {

                @SerializedName("comments")
                private Comments comments;

                public Comments getComments() {
                    return comments;
                }

                public void setComments(Comments comments) {
                    this.comments = comments;
                }

                public class Comments {

                    @SerializedName("nodes")
                    private List<Node> nodes;

                    public List<Node> getNodes() {
                        return nodes;
                    }

                    public void setNodes(List<Node> nodes) {
                        this.nodes = nodes;
                    }

                    public class Node {

                        @SerializedName("text")
                        private String text;

                        @SerializedName("user")
                        private User user;

                        public String getText() {
                            return text;
                        }

                        public void setText(String text) {
                            this.text = text;
                        }

                        public User getUser() {
                            return user;
                        }

                        public void setUser(User user) {
                            this.user = user;
                        }

                        public class User {

                            @SerializedName("username")
                            private String username;

                            public String getUsername() {
                                return username;
                            }

                            public void setUsername(String username) {
                                this.username = username;
                            }
                        }
                    }
                }
            }
        }
    }
}
