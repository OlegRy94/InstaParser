package com.olegry.instaparser.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by olegrabcev on 15.04.16.
 */
public class InstagramAccountInfo {

    @SerializedName("entry_data")
    private EntryData entryData;

    public EntryData getEntryData() {
        return entryData;
    }

    public void setEntryData(EntryData entryData) {
        this.entryData = entryData;
    }

    public class EntryData {

        @SerializedName("ProfilePage")
        private List<ProfilePage> profilePage;

        public List<ProfilePage> getProfilePage() {
            return profilePage;
        }

        public void setProfilePage(List<ProfilePage> profilePage) {
            this.profilePage = profilePage;
        }

        public class ProfilePage {

            @SerializedName("user")
            private User user;

            public User getUser() {
                return user;
            }

            public void setUser(User user) {
                this.user = user;
            }

            public class User {

                @SerializedName("media")
                private Media media;

                @SerializedName("username")
                private String username;

                public Media getMedia() {
                    return media;
                }

                public void setMedia(Media media) {
                    this.media = media;
                }

                public String getUsername() {
                    return username;
                }

                public void setUsername(String username) {
                    this.username = username;
                }

                public class Media {

                    @SerializedName("nodes")
                    private List<Node> nodes;

                    public List<Node> getNodes() {
                        return nodes;
                    }

                    public void setNodes(List<Node> nodes) {
                        this.nodes = nodes;
                    }

                    public class Node {

                        @SerializedName("code")
                        private String code;

                        @SerializedName("comments")
                        private Comments comments;

                        public String getCode() {
                            return code;
                        }

                        public void setCode(String code) {
                            this.code = code;
                        }

                        public Comments getComments() {
                            return comments;
                        }

                        public void setComments(Comments comments) {
                            this.comments = comments;
                        }

                        public class Comments {

                            @SerializedName("count")
                            private int count;

                            public int getCount() {
                                return count;
                            }

                            public void setCount(int count) {
                                this.count = count;
                            }
                        }
                    }
                }
            }
        }
    }
}
