package com.olegry.instaparser.model;

import java.util.List;

/**
 * Created by olegrabcev on 03.07.16.
 */
public class OutputUserData {

    private String username;
    private List<Comment> comments;

    public OutputUserData(String username, List<Comment> comments) {
        this.username = username;
        this.comments = comments;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("https://www.instagram.com/%s : \n", username));
        for (Comment comment : comments) {
            builder.append(String.format("\t %s : %s \n", comment.getAuthor(), comment.getText()));
        }
        return builder.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OutputUserData) {
            OutputUserData another = (OutputUserData) obj;
            return this.username.equals(another.username);
        }
        return super.equals(obj);
    }
}
