package com.olegry.instaparser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by olegrabcev on 15.04.16.
 */
public class Reader {

    public static List<String> readFile() throws IOException {
        List<String> nicknames = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("input.txt")));

        String line;
        while ((line = reader.readLine()) != null) {
            String[] accountData = line.split(":");

            String nickname = accountData[0];
            nicknames.add(nickname);
        }

        return nicknames;
    }
}
