package com.olegry.instaparser;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.olegry.instaparser.model.Comment;
import com.olegry.instaparser.model.InstagramAccountInfo;
import com.olegry.instaparser.model.InstagramPhotoInfo;
import com.olegry.instaparser.model.OutputUserData;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by olegrabcev on 15.04.16.
 */
public class Parser {

    private static final String PHOTO_SELECTOR = "script";

    private static List<OutputUserData> results = new ArrayList<>();

    public static void parse() {
        List<String> nicknames = null;
        try {
            nicknames = Reader.readFile();
            nicknames.stream().forEach(n -> parse(n));
            write();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void parse(String nickname) {
        String url = "https://www.instagram.com/" + nickname + "/";

        try {
            Document doc = Jsoup.connect(url)
                    .timeout(15000)
                    .userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2").get();
            Elements photos = doc.select(PHOTO_SELECTOR);
            photos.stream().forEach(elem -> checkElement(elem));
        } catch (IOException e) {
            System.out.println(nickname + " not found! " + e.getMessage());
        }
    }

    private static void write() throws FileNotFoundException {
        PrintWriter out = new PrintWriter("output.txt");
        results.stream().forEach(u -> writeObject(u, out));
        out.close();
    }

    private static void writeObject(OutputUserData userData, PrintWriter out) {
        out.println(userData.getUsername());
        userData.getComments().stream().forEach(c -> out.println(String.format("\t %s : %s", c.getAuthor(), c.getText())));
        out.println();
    }

    private static void checkElement(Element elem) {
        if (elem.childNodes() == null || elem.childNodes().size() == 0) return;
        List<Node> childNodes = elem.childNodes();
        for (Node childNode : childNodes) {
            if (childNode.toString().contains("window._sharedData")) {
                getCommentsCount(childNode.toString());
                return;
            } else continue;
        }
    }

    private static void getCommentsCount(String child) {
        String[] data = child.split(" = ");
        if (data.length > 1) {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(data[1]));
            reader.setLenient(true);
            InstagramAccountInfo instagramAccountInfo = gson.fromJson(reader, InstagramAccountInfo.class);
            if (instagramAccountInfo != null) {
                List<InstagramAccountInfo.EntryData.ProfilePage.User.Media.Node> nodes = instagramAccountInfo.getEntryData().getProfilePage().get(0).getUser().getMedia().getNodes();
                if (nodes == null) return;
                for (InstagramAccountInfo.EntryData.ProfilePage.User.Media.Node node : nodes) {
                    if (node.getComments().getCount() > 0) {
                        String username = instagramAccountInfo.getEntryData().getProfilePage().get(0).getUser().getUsername();
                        String code = node.getCode();
                        List<Comment> comments = parseComments(username, code);
                        OutputUserData outputUserData = new OutputUserData(username, comments);
                        if (results.contains(outputUserData)) {
                            OutputUserData existingData = results.get(results.indexOf(outputUserData));
                            existingData.getComments().addAll(outputUserData.getComments());
                        } else {
                            results.add(outputUserData);
                        }
                    }
                }
            }
        } else return;
    }

    private static List<Comment> parseComments(String username, String code) {
        // "https://www.instagram.com/p/BCgQfSyjmnG/?taken-by=p20__renata__191"
        String url = String.format("https://www.instagram.com/p/%s/?taken-by=%s", code, username);
        try {
            Document document = Jsoup.connect(url).timeout(5000).get();
            Elements posts = document.select(PHOTO_SELECTOR);
            for (Element post : posts) {
                List<Comment> comments = checkPost(post);
                if (comments != null) return comments;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static List<Comment> checkPost(Element post) {
        if (post.childNodes() == null || post.childNodes().size() == 0) return null;
        List<Node> childNodes = post.childNodes();
        for (Node childNode : childNodes) {
            if (childNode.toString().contains("window._sharedData")) {
                List<Comment> comments = getComments(childNode.toString());
                return comments;
            }
        }
        return null;
    }

    private static List<Comment> getComments(String child) {
        String[] data = child.split(" = ");
        if (data.length > 1) {
            Gson gson = new Gson();
            JsonReader reader = new JsonReader(new StringReader(data[1]));
            reader.setLenient(true);
            InstagramPhotoInfo instagramPhotoInfo = gson.fromJson(reader, InstagramPhotoInfo.class);
            if (instagramPhotoInfo != null) {
                List<InstagramPhotoInfo.EntryData.PostPage.Media.Comments.Node> nodes = instagramPhotoInfo.getEntryData().getPostPage().get(0).getMedia().getComments().getNodes();
                if (nodes == null) return null;
                List<Comment> comments = new ArrayList<>();
                for (InstagramPhotoInfo.EntryData.PostPage.Media.Comments.Node node : nodes) {
                    Comment comment = new Comment(node.getUser().getUsername(), node.getText());
                    comments.add(comment);
                }
                return comments;
            }
        }
        return null;
    }
}
